from rest_framework import serializers

from core.models import Skill


class SkillSerializer(serializers.ModelSerializer):
    """ Serializer for tag objects """

    class Meta:
        model = Skill
        fields = '__all__'
