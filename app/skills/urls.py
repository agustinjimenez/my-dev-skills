from rest_framework import routers
from rest_framework.routers import DefaultRouter

from django.urls import path, include

from skills.views import SkillViewSet


class MyDevSkills(routers.APIRootView):
    """
    This public API was developed in order to show you my IT skills.

    Manteined by Agustin Jimenez <agustin_jimeez@tutanota.com>.
    """
    pass


class DocumentedRouter(routers.DefaultRouter):
    APIRootView = MyDevSkills


app_name = 'skills'
router = DocumentedRouter()
router.register('', SkillViewSet)

urlpatterns = [
               path('',include(router.urls)),
              ]
