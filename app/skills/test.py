from django.test import TestCase
from django.urls import reverse

from rest_framework import status
from rest_framework.test import APIClient

from core.models import Skill, Category
from skills.serializers import SkillSerializer


LIST_SKILLS_URL = reverse('skills:skill-list')


class TestSkills(TestCase):
    """ Test Views, Serializers and Urls of skills endpoint """

    def setUp(self):
        """ Initialize variables for all tests cases
            :return: None
        """
        self.client = APIClient()

        return None

    def test_listing_skills_successful(self):
        """ Test that 'list skills' url is aviable
            :return: None
        """
        category = Category.objects.create(name='Python')
        Skill.objects.create(
                             name='Pytest',
                             category=category
                            )
        Skill.objects.create(
                             name='Django',
                             category=category
                            )


        skills = Skill.objects.all().order_by('-name')
        serializer = SkillSerializer(skills, many=True)

        response = self.client.get(LIST_SKILLS_URL)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)
        return None

    def test_read_only_endpoint(self):
        """ Test that 'skills/' endpoint is read only
            :return: None
        """
        response = self.client.post(LIST_SKILLS_URL, data={})

        self.assertEqual(
                         response.status_code,
                         status.HTTP_405_METHOD_NOT_ALLOWED
                        )
        return None
