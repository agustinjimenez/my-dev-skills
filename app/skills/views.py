from rest_framework import viewsets

from core.models import Skill
from skills import serializers

class SkillViewSet(viewsets.ReadOnlyModelViewSet):
    """ Skills endpoint. Contains name, about, estimate_level,
    category,  commentary and tags """
    queryset = Skill.objects.all()
    serializer_class = serializers.SkillSerializer
