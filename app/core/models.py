from django.db import models
from taggit.managers import TaggableManager


# Abstract Models --------------

class Level(models.Model):
    """ Abstract model looking for evaluate skills and groups of
        skills
    """
    name = models.CharField(
                            blank=False,
                            max_length=30,
                            unique=True
                            )
    LEVEL_CHOICES = (
                    (0, 'Work in progress'),
                    (1, 'Basic'),
                    (2, 'Medium'),
                    (3, 'Advanced'),
                    )
    estimate_level = models.PositiveIntegerField(
					verbose_name='Estimate level',
                                        choices=LEVEL_CHOICES,
                                        default=1
                                        )
    class Meta:
        abstract = True

    def __str__(self):
        return str(self.name)


# Concret Models ----------------

class Category(Level):
    """ Groups of skills """
    pass


class Skill(Level):
    """ Skill model """
    about = models.URLField(verbose_name='Whas is it?')
    comment = models.TextField(verbose_name='Commentaries')
    category = models.ForeignKey(
                                 'Category',
                                 related_name='skills',
                                 on_delete=models.CASCADE,
                                  blank=False
                                 )
    tags = TaggableManager()
