from unittest.mock import patch

from django.core.management import call_command
from django.db.utils import OperationalError
from django.test import TestCase


class TestCommands(TestCase):
    """ core/managment/commands/ test suite """

    def wait_for_db_ready(self):
        """ Waiting for db is available
            :return: None
        """
        with patch('django.db.utils.ConnectionHandler.__getitem__')as gi:
            gi.return_value = True
            call_command('wait_for_db')

            self.assertEqual(gi.call_count, 1)
            return None


    @patch('time.sleep', return_value=None)
    def test_wait_for_db(self, ts):
        """ Test wait_for_db function
            :ts: give a function from patch 'time.sleep'
            :return: None
        """
        with patch('django.db.utils.ConnectionHandler.__getitem__')as gi:
            gi.side_effect = [OperationalError] *5 + [True]
            call_command('wait_for_db')

            self.assertEqual(gi.call_count, 6)
            return None
