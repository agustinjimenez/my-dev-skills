from django.test import TestCase
from core import models

class TestStringRepresentation(TestCase):
    """ Test the string representation of models """

    def test_category(self):
        """ Test str conversion of category model
            :return: None
        """
        categoryName = 'Test_category_name'
        testCategory = models.Category.objects.create(name=categoryName)

        self.assertEqual(categoryName, str(testCategory))
        return None

    def test_skill(self):
        """ Test str conversion of skill model
            :return: None
        """
        testCategory = models.Category.objects.create(name='someName')
        skillName = 'Test_category_name'
        testSkill = models.Skill.objects.create(
                                                name=skillName,
                                                category=testCategory
                                                )

        self.assertEqual(skillName, str(testSkill))
        return None
