from core.models import Category, Skill

import csv


CATEGORIES = {
              'Development': 2,
                  'Testing': 1,
               'Deployment': 1,
              'Mathematics': 1
             }


class HelperFunctins():

    """ Interface with helpful methods """

    def create_categories(self, categoriesDictionary):
        """ Create categories before the skills
            :categoriesDictionary: a dictionary of name of each
                                    each category and estimate_level
            :return: None
        """
        for category, level in categoriesDictionary.items():
            eval('Category.objects.create(name="{}",estimate_level={})'\
                 .format(
                         category,
                         level
                        ))

        return None

    def get_object(self, model, name):
        """ Return a specific Objet model from DataBase

            :model: String representation of a Model
            :name: Name of specific table on database
            :return: Object Model
        """
        if not model:
            return ''
        return eval('{}.objects.get(name="{}")'.format(
                                                    model,
                                                    name
                                                  ))


class Loader(HelperFunctins):

    """ Instance for loading data into DataBase """

    def __init__(self, path):
        self.path = path
        self.read_data_from_csv_file()

    def read_data_from_csv_file(self):
        """ Store data from csv file into data property
            :return: None
        """
        self.data = []
        with open(self.path) as f:
            reader = csv.reader(f)
            for row in reader:
                self.data.append(row)

    def load(self):
        """ Load data into DataBase
            :return: None
        """
        self.create_categories(CATEGORIES)

        for row in self.data:

            try:
                skill = Skill.objects.create(
                            name=row[0],
                            about=row[1],
                            comment=row[2],
                            category=self.get_object('Category',row[3]),
                            estimate_level=row[4],
                            tags=row[5]
                           )
                skill.save()
            except:
                print('LoadError:',
                      'May bad data is placed in following line',
                      row
                      )
        return None
