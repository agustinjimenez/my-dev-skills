from io import StringIO

from django.core.management import call_command
from django.test import TestCase

class TestCommands(TestCase):

    """ Test commands from load_data module """

    def test_load_data_management_command(self):
        """ Test that load_data command not raises error
            :return: None
        """
        out = StringIO()
        call_command('load_data', stdout=out)

        self.assertIn('The data was successfuly loaded',
                         out.getvalue()
                        )
        return None
