from load_data.load import Loader
from core.models import Skill, Category

from django.test import TestCase

import csv
from random import randint


TEST_CSV_FILE = '/tmp/test_loading_data.csv'

def create_test_csv_file(path):
    with open(TEST_CSV_FILE, 'w') as f:
        writer = csv.writer(f)
        for i in range(100):
            writer.writerow([
                            str(i),
                            'http://google.com/',
                            'some covenient commentary',
                            'Development',
                            str(randint(0,3)),
                            'Tag1, Tag2, Tag3'
                            ])



class TestLoadData(TestCase):

    """ Test loading data from csv file to database """

    def setUp(self):
        """ Initialize variables for each test case
            :return: None
        """
        create_test_csv_file(TEST_CSV_FILE)
        self.loader = Loader(TEST_CSV_FILE)

        return None

    def clear_database(self):
        Skill.objects.all().delete()

    def test_get_object_with_correct_input(self):
        """ Test that get_object function return
            :return: None
        """
        test_category = Category.objects.create(name='TestCategory')
        test_category.save()
        output1 = self.loader.get_object('Category','TestCategory')

        self.assertEqual(output1, test_category)
        return None

    def test_get_object_with_void_input(self):
        """ Test that get_object function return
            :return: None
        """
        output = self.loader.get_object('', None)

        self.assertEqual(output, '')
        return None

    def test_reading_csv_file(self):
        """ Test that Loader object reads csv file successful
            :return None:
        """
        self.assertEqual(self.loader.data[0][0], '0')
        self.assertEqual(self.loader.data[1][0], '1')
        return None

    def test_loading_data_successfuly(self):
        """ Test that load method works
            :return: None
        """
        self.loader.load()
        TestSkill1_exist = Skill.objects.filter(name='1').exists()
        TestSkill99_exist = Skill.objects.filter(name='99').exists()
        skill_count = Skill.objects.count()

        self.assertTrue(TestSkill1_exist)
        self.assertTrue(TestSkill99_exist)
        self.assertEqual(skill_count, 100)
        self.clear_database()
        return None

    def test_loading_data_with_blank_arguments(self):
        """ Test that Loader.load() not raises error if in the csv file
            are empty strings.
            :return: None
        """

        # Create a new csv file with empty strings
        with open('/tmp/new_csv_file_for_testing.csv', 'w') as f:
            writer = csv.writer(f)
            for i in range(100):
                writer.writerow([
                                 str(i),
                                 '',
                                 '',
                                 '',
                                 'Development',
                                 '',
                                 ''
                               ])

        # Initialize variables for assertions
        loader = Loader(TEST_CSV_FILE)
        loader.load()
        TestSkill1_exist = Skill.objects.filter(name='1').exists()
        TestSkill99_exist = Skill.objects.filter(name='99').exists()
        skill_count = Skill.objects.count()

        self.assertTrue(TestSkill1_exist)
        self.assertTrue(TestSkill99_exist)
        self.assertEqual(skill_count, 100)
        self.clear_database()
        return None

    def test_loading_data_with_definitive_csv_file(self):
        """ Test that Loader object with csv file used in production
            :return: None
        """
        loader = Loader('load_data/skills.csv')
        loader.load()

        TestSkillDjango_exist = Skill.objects.filter(
                                                name='Django').exists()
        TestSkillPython_exist = Skill.objects.filter(
                                                name='Python').exists()

        self.assertTrue(TestSkillPython_exist)
        self.assertTrue(TestSkillDjango_exist)
        return None
