from django.core.management.base import BaseCommand, CommandError
from load_data.load import Loader

CSV_FILE = 'load_data/skills.csv'

class Command(BaseCommand):
    help = 'Load data from a csv file'

    def handle(self, *args, **options):
        try:
            loader = Loader(CSV_FILE)
            loader.load()
        except:
            raise CommandError(
                    'LoadError: May bad data is placed in {}'.format(
                                                                CSV_FILE
                                                              ))
        self.stdout.write(
                    self.style.SUCCESS(
                        'The data was successfuly loaded'
                ))
        return None
