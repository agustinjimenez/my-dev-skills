from django.urls import path, include
from rest_framework.documentation import include_docs_urls

DOCUMENTAION_API_TITLE = 'My Dev Skills'
DOCUMENTATION_API_DESCRIPTION= """
SKILL LEVELS:
    0: 'Work in progress'
    1: 'Basic'
    2: 'Medium'
    3: 'Advanced'
"""

urlpatterns = [

    path('', include_docs_urls(
                    title=DOCUMENTAION_API_TITLE,
                    description= DOCUMENTATION_API_DESCRIPTION
                )),

    path('categories/', include('categories.urls')),
    path('skills/', include('skills.urls')),
]
