from rest_framework import routers
from rest_framework.routers import DefaultRouter

from django.urls import path, include

from categories.views import CategoryViewSet


class MyDevSkills(routers.APIRootView):
    """
    This public API was developed in order to show you my IT skills.

    Manteined by Agustin Jimenez <agustin_jimeez@tutanota.com>.
    """
    pass


class DocumentedRouter(routers.DefaultRouter):
    APIRootView = MyDevSkills


app_name = 'categories'
router = DocumentedRouter()
router.register('', CategoryViewSet)

urlpatterns = [
               path('',include(router.urls)),
              ]
