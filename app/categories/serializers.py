from rest_framework import serializers

from core.models import Category, Skill


class CategorySerializer(serializers.ModelSerializer):

    """ Serializer for tag objects """

    skills = serializers.StringRelatedField(many=True)

    class Meta:
        model = Category
        fields = ['name', 'estimate_level', 'skills']
