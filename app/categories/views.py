from rest_framework import viewsets

from core.models import Category
from categories import serializers

class CategoryViewSet(viewsets.ReadOnlyModelViewSet):
    """ Each Category contains a name, a estimate level and the skills
        placed in its.
    """
    queryset = Category.objects.all()
    serializer_class = serializers.CategorySerializer
