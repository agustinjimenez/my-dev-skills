from django.test import TestCase
from django.urls import reverse

from rest_framework import status
from rest_framework.test import APIClient

from core.models import Category
from categories.serializers import CategorySerializer

LIST_CATEGORIES_URL = reverse('categories:category-list')

class TestCategories(TestCase):
    """ Test Views, Serializers and Urls of categories endpoint """

    def setUp(self):
        """ Initialize variables for all tests cases
            :return: None
        """
        self.client = APIClient()

        return None

    def test_listing_categories_successful(self):
        """ Test that 'list categories' url is aviable
            :return: None
        """
        Category.objects.create(name='Python')
        Category.objects.create(name='Mathematics')

        categories = Category.objects.all().order_by('-name')
        serializer = CategorySerializer(categories, many=True)

        response = self.client.get(LIST_CATEGORIES_URL)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)
        return None

    def test_read_only_endpoint(self):
        """ Test that 'categories/' endpoint is read only
            :return: None
        """
        response = self.client.post(LIST_CATEGORIES_URL, data={})

        self.assertEqual(
                         response.status_code,
                         status.HTTP_405_METHOD_NOT_ALLOWED
                        )
        return None
