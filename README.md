# My Dev Skills

There are an API in order to show my knowledges.
I orderin my skills for 4 categories:
- Development
- Testing
- Deployment
- Mathematics

Each skill and each category contains a number between 0 and 3 that
represents the level that I consider to have.

#### WARNING
The swarm support is work in progress. This repository needs to be a
orchestrated cluster to grow up at the next level. After Swarm support,
the next features are:
- Pandas and Matplotlib integration
- Celery asyncronous retorning pdfs
- Redis caching and queryset optimisations

<br>

0. 'Work in progress'
1. Basic
2. Medim
3. Advanced

<br>
<br>
![Image1](img/docs.png)

---

## Runing the API
### Alternative 1: Git + Docker-Compose
~~~sh
git clone git@gitlab.com/agusitnjimenez/my-dev-skills.git
doker pull registry.gitlab.com/agustinjimenez/my-dev-skills:latest
docker compose up
~~~

### Alternative 2: A Docker image
~~~sh
docker run -d -p 8000:8000 registry.gitlab.com/agustinjimenez/my-dev-skills:0.0.1
~~~

#### Alternative 3: Git + Django Development Server
#### This Choise requires activate Virtual Environment

~~~sh
git clone https://gitlab.com/agustinjimenez/my-dev-skills
cd my-dev-skills
virtualenv django3.5_restFramework3.11
source virtualenv/bin/activate
pip install -r requirements.txt
python app/manage.py runserver 127.0.0.1:8000
~~~

---

![Image2](img/get_skills.png)

## Endpoints

- API DOCUMENTATION
    <br>
    GET: 127.0.0.1:8000/
- SKILLS
    <br>
    GET: 127.0.0.1:8000/skills/
- CATEGORIES OF SKILLS
    <br>
    GET: 127.0.0.1:8000/categories/

<br>

## Data in text file
You could see the last version of all data [HEREA](https://gitlab.com/agustinjimenez/my-dev-skills/-/blob/cea2cf0c60364d38a1676ef25479945ef3cc2db9/data.json)

---

<br>

#### License
This project is licensed under the MIT License.
See more in the [LICENSE](LICENSE)


#### Contributing
Contrubutors are wellcome! See [CONTRIBUTING.md](CONTRIBUTING.md)

#### Authors
Development lead and mainteined by [Agustin Jimenez](http://agustinjimenez.gitlab.io/portfolio) 
<agustin.j@zohomail.com>
