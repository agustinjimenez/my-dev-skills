#!/bin/bash
set -x

# Delete Keystore
docker-machine rm aws-MDevS-keystore
# Delete master node
docker-machine rm -y MDevS_master &
# Delete all other nodes
for server in {1..2}; do
docker-machine rm -y MDevS_${server} &
done

# delete all storage in DO (be sure you are ok deleting ALL storage in an account)
# doctl compute volume ls --format ID --no-header | while read -r id; do doctl compute volume rm -f "$id"; done




