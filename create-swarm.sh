#!/bin/bash
# set -x

# All documentations about docker machine parameters and environment
# variables are here:
#       https://docs.docker.com/machine/drivers/aws/


# Check environment variables
    if [ -z "$AWS_ACCESS_KEY_ID"  ] 
        || [ -z "$AWS_SECRET_ACCESS_KEY" ] 
        || [ -z "$AWS_VPC_ID" ]; then
      echo "ERROR: Enviroment Variables not found"
      echo "Please set following environment variables:
                - AWS_ACCESS_KEY_ID, 
                - AWS_SECRET_ACCESS_KEY
                - AWS_VPC_ID"
      exit 1;
    fi


# Create multi host keystore
    docker-machine create \
        --driver amazonec2 \
        --amazonec2-access-key $AWS_ACCESS_KEY_ID 
        --amazonec2-secret-key $AWS_SECRET_ACCESS_KEY 
        --amazonec2-vpc-id $AWS_VPC_ID \
        --engine-opt dns=8.8.8.8 \
        aws-MDevS-keystore

    eval "$(docker-machine env aws-MDevS-keystore)"

    docker run -d -p "8500:8500" \
        -h "consul"  \
        progrium/consul \
        -server \
        -bootstrap


# Create node master
    docker-machine create \
    --driver amazonec2 
    --amazonec2-access-key $AWS_ACCESS_KEY_ID \
    --amazonec2-secret-key $AWS_SECRET_ACCESS_KEY \
    --engine-opt dns=8.8.8.8 \
    --amazonec2-vpc-id $AWS_VPC_ID \
    --swarm \
    --swarm-master \
    --swarm-strategy "spread" \
    --swarm-discovery="consul://$(docker-machine ip aws-MDevS-keystore):8500" \
    --engine-opt="cluster-store=consul://$(docker-machine ip aws-MDevS-keystore):8500" \
    --engine-opt="cluster-advertise=eth0:2376" \
    MDevS_master


# Create other nodes
    for node in {1..2}; do
        docker-machine create 
            --driver amazonec2 \
            --amazonec2-access-key $AWS_ACCESS_KEY_ID \
            --amazonec2-secret-key $AWS_SECRET_ACCESS_KEY \
            --amazonec2-vpc-id $AWS_VPC_ID \
            --engine-opt dns=8.8.8.8 \
            --swarm 
            --swarm-discovery="consul://$(docker-machine ip aws-MDevS-keystore):8500" 
            --engine-opt="cluster-store=consul://$(docker-machine ip aws-MDevS-keystore):8500" 
            --engine-opt="cluster-advertise=eth0:2376"
            MDevS_$node
    done
