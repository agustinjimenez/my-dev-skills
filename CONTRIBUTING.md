# CONTRIBUTORS ARE WELLCOME!

**Any help will be appreciated**. You can develop new features, fix bugs, 
improve the documentation, or do some other cool stuff.

If you have new ideas or want to complain about bugs, feel free to 
create a new issue.

Please, send me an email if you have other needs about this project:
agustin_jimenez@tutanota.com

Thank You!
